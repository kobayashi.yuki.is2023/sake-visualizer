# SAKE Visualizer

## 全体像
![全体構造.drawio](/uploads/b03a468e58167e2573eed8cb783b4c6c/全体構造.drawio__1_.png)

## 開発環境
- ソースコードエディタ : Visual Studio Code  
- バージョン・コード管理 : Gitlab(ハッカソンのアカウントで登録お願いします。)  
- JavaScript実行環境 :Node.js (v16.13.0)
- 開発言語 : TypeScript


## 開発環境の構築
### 1. **Git**のインストール(インストールしてある場合は不要)  
[Git](https://git-scm.com/)のリンクからインストール。SetupはすべてNextを押してもらって大丈夫です。デフォルトで動作します。  
一度PCを再起動し、Visual Studio Codeを開く。以下のように`git`と打った際にエラーが吐かれず認識されてればok。
```
PS C:\Users\Yuki\Desktop> git 
usage: git [--version] [--help] [-C <path>] [-c <name>=<value>]
           [--exec-path[=<path>]] [--html-path] [--man-path] [--info-path]
           [-p | --paginate | -P | --no-pager] [--no-replace-objects] [--bare]
           [--git-dir=<path>] [--work-tree=<path>] [--namespace=<name>]
           [--super-prefix=<path>] [--config-env=<name>=<envvar>]
           <command> [<args>]

These are common Git commands used in various situations:

. 
.
.

'git help -a' and 'git help -g' list available subcommands and some
concept guides. See 'git help <command>' or 'git help <concept>'
to read about a specific subcommand or concept.
See 'git help git' for an overview of the system.
```

### 2. **Node.js**のインストール  
[Node.js](https://nodejs.org/ja/)のインストールページからインストール。(バージョンは推奨版の16.13.0LTSで良いと思います。)
以下のようにコマンドを打った際に認識されていればok。
```
PS C:\Users\Yuki\Desktop> node -v
v16.13.0
```

### 3. **yarn**のインストール
`yarn`とはnodeのパッケージマネージャーであり、基本この`yarn`を用いてパッケージをインストールしていく。node.jsをインストールすることで`npm`というパッケージマネージャーが利用できるようになるので、これを用いて以下のようにコマンドを打ち込む。
```
PS C:\Users\Yuki\Desktop> npm install --global yarn
```
`yarn --version`でバージョンが出てくればok。
```
PS C:\Users\Yuki\Desktop> yarn --version
1.22.17
```

### 4. Reactプロジェクトを作る(SAKE-visualizerでは既に作ってあるので、ここは練習として)  
`create-react-app`を用いることで、簡単にReact + TypeScript(JavaScript)の開発環境を構築できる。  
`npx create-react-app --version`と打ち込むことで、必要なパッケージをインストールできる。(npxはnpmパッケージを簡単に実行できるコマンド。多分ここくらいしか使わないかも)
```
PS C:\Users\Yuki\Desktop> npx create-react-app --version
Need to install the following packages:
  create-react-app
Ok to proceed? (y) y
npm WARN deprecated tar@2.2.2: This version of tar is no longer supported, and will not receive security updates. Please upgrade asap.
4.0.3
```
次に、`npx create-react-app {プロジェクト名} --template typescript`と打ち込むことで、typescriptのテンプレプロジェクトが生成される。ここでは`demo`という名前でプロジェクトを作成して進めていく。
```
PS C:\Users\Yuki\Desktop> npx create-react-app demo --template typescript           

Creating a new React app in C:\Users\Yuki\Desktop\demo.

Installing packages. This might take a couple of minutes.
Installing react, react-dom, and react-scripts with cra-template-typescript...

.
.
.

  cd demo
  yarn start

Happy hacking!
```
プロジェクトのディレクトリに移動し、`yarn start`と打つとテンプレートのwebアプリがローカルで開く。
```
PS C:\Users\Yuki\Desktop> cd demo
PS C:\Users\Yuki\Desktop\demo> yarn start
```
![react_demo_page](/uploads/d0d5293950e2f564518f377fa3d6e1e1/react_demo_page.png)

##### ここまでで
ここまでできればひとまず開発はできると思います。プロジェクト内のApp.tsx等をいじってみてWebアプリ側の挙動を確認してみてください。  

ReactとTypeScriptで勉強するところが多くなってしまい大変申し訳ないです...  
Reactに関しては、時間があるようであれば公式のドキュメントをたどってとりあえず動かしてみるのが一番かなと思います。検索かけてもらえるとたくさんの記事がありますので、Reactで何ができるのか、大まかにどんな仕組みなのかはわかるかもしれません。 自分もわかる範囲でお伝えできればと思うので、いつでも連絡ください。

TypeScriptに関してはJavaScriptの型定義しっかりバージョンです。　　
[TypeScript の概要](https://qiita.com/EBIHARA_kenji/items/4de2a1ee6e2a541246f6)  
習うより慣れよ精神で触っていただければと思います。 いろいろエラー出ると思いますが、ある程度は小林が同じところつまずいていたはずなので聞いてもらえれば一緒に解決します!  

### 5. Gitlabからデータのクローン
#### クローン(gitlab上のプロジェクトをローカル環境に持ってくる)
まずGitlabからプロジェクトのクローンを行う。1.でgitがインストールされているはずなので、visual studio codeのターミナルを開いて任意のフォルダで`git clone`を行う。今回はDesktopディレクトリ上にプロジェクトをクローンする。  
下画像のようにgitlabプロジェクトのcloneボタンをクリックすると`HTTPSでクローン`という文字が出てくる。
![gitlab_clone](/uploads/36096fd6e2d2262db3c2e293dbe83d77/gitlab_clone.png)  
ここのURLをコピーして、VS codeのターミナルで以下コマンド`git clone https://gitlab.com/kobayashi.yuki.is2023/sake-visualizer.git`を実行
```
PS C:\Users\Yuki\Desktop> git clone https://gitlab.com/kobayashi.yuki.is2023/sake-visualizer.git
Cloning into 'sake-visualizer'...
remote: Enumerating objects: 63, done.
remote: Counting objects: 100% (63/63), done.
remote: Compressing objects: 100% (48/48), done.
remote: Total 63 (delta 19), reused 28 (delta 13), pack-reused 0
Receiving objects: 100% (63/63), 305.48 KiB | 547.00 KiB/s, done.
Resolving deltas: 100% (19/19), done.
```
このようにdoneまで行けばプロジェクトをローカルにクローンしてくることに成功している。

##### ここまでで
もし以下のような形でCloneに失敗した場合、クローンに失敗する場合の原因の一つとして、gitlabのパスワードの再設定が済んでいないことが挙げられます。パスワードを再設定するとうまくクローン出来るはずです。
```
PS C:\Users\Yuki\Desktop> git clone https://gitlab.com/kobayashi.yuki.is2023/sake-visualizer.git
Cloning into 'sake-visualizer'...
remote: HTTP Basic: Access denied
fatal: Authentication failed for 'https://gitlab.com/kobayashi.yuki.is2023/sake-visualizer.git/'
```

クローン出来たら、一度プロジェクトが実行できてWebアプリが開くかどうか確認してみてください。
```
PS C:\Users\Yuki\Desktop> cd .\sake-visualizer\
PS C:\Users\Yuki\Desktop\sake-visualizer> cd .\sake-visualizer\
```
初回クローン時はパッケージがインストールされていないです。そこで`yarn`と打ち込んむことで必要なパッケージをインストールできます。
```
PS C:\Users\Yuki\Desktop\sake-visualizer\sake-visualizer> yarn 
```
インストールが終われば`yarn start`でローカルでアプリが立ち上がります。
```
PS C:\Users\Yuki\Desktop\sake-visualizer\sake-visualizer> yarn start
```

#### gitのuser email,nameの設定
コミットやプッシュを行う際emailとnameを設定しないと動作しないので、以下のように設定を行う。
```
PS C:\Users\Yuki\Desktop\sake-visualizer> git config --global user.email "kobayashi.yuki.is2023@gmail.com"
PS C:\Users\Yuki\Desktop\sake-visualizer> git config --global user.name "Kobayashi Yuki"    
```

### 6. firebaseとの連携
ここでは**demo**プロジェクトを用いて連携手順を示す。
#### 6.1. firebaseツールのインストール
プロジェクトのディレクトリに移動し、`firebase-tools`をインストール
```
PS C:\Users\Yuki\Desktop> cd demo
PS C:\Users\Yuki\Desktop\demo> yarn add firebase-tools
```
#### 6.2. firebase上にプロジェクトを作成
ブラウザでfirebaseと検索し、検索結果上部の[firebase](https://firebase.google.com/?hl=ja)をクリックする。
![firebase-browser](/uploads/95d2744a318c60472e965cd9e0f19a04/firebase-browser.png)
開くと以下のような画面が出てくると思うので、「使ってみる」ボタンを押す。  
![firebase-home](/uploads/e679727e63f21d4cfdff614043a18bd8/firebase-home.png)  
自身のアカウントでログインしていると以下のようなFirebaseプロジェクトが見れるので、「プロジェクトを追加」より新規プロジェクトを作成する。
![firebase-project-page](/uploads/4091e50e9adb32e11e5f34e27e4a5ab1/firebase-project-page.png)
プロジェクト名は任意で、Googleアナリティクスは現段階では使わないので有効無効どちらでも大丈夫です。  
以下のようなページに飛べば作成成功です。  
![firebase-demo-home](/uploads/029c3a6ee1e0154fbe565ca475f12a52/firebase-demo-home.png)  

次にプロジェクト内の`firestore`を使えるように設定する。
(まだ書き途中...)

#### 6.3. VS codeからfirebaseへのログインを行う  
VScodeのターミナルからfirebaseへのログインを行っていく。  
まず4.で作成したプロジェクトのディレクトリに入る。
(このプロジェクトには6.1.の`firebase-tools`がインストールされているようにしてください。)  
```
PS C:\Users\Yuki\Desktop> cd demo
```
次にfirebaseのログインを行う。最初のY/nはyでいいです。
```
PS C:\Users\Yuki\Desktop\demo> yarn firebase login
yarn run v1.22.17
$ C:\Users\Yuki\Desktop\demo\node_modules\.bin\firebase login
i  Firebase optionally collects CLI usage and error reporting information to help improve our products. Data is collected in accordance with Google's privacy policy (https://policies.google.com/privacy) and is not used to identify you.

? Allow Firebase to collect CLI usage and error reporting information? (Y/n) y
```
認証のためブラウザに飛ぶと思うので、6.2.で作成したプロジェクトのあるアカウントでログインする。  
うまく認証されたら以下のような文がターミナルで出てきます。
```
+  Success! Logged in as kobayashi.yuki.is2023@gmail.com
Done in 176.73s.
```
次にfirebaseプロジェクトを作成する。`yarn firebase init`と打ち込むと以下のような文が吐かれるので、yを押して続行。
```
PS C:\Users\Yuki\Desktop\demo> yarn firebase init
yarn run v1.22.17
$ C:\Users\Yuki\Desktop\demo\node_modules\.bin\firebase init

     ######## #### ########  ######## ########     ###     ######  ########
     ##        ##  ##     ## ##       ##     ##  ##   ##  ##       ##
     ######    ##  ########  ######   ########  #########  ######  ######
     ##        ##  ##    ##  ##       ##     ## ##     ##       ## ##
     ##       #### ##     ## ######## ########  ##     ##  ######  ########

You're about to initialize a Firebase project in this directory:

  C:\Users\Yuki\Desktop\demo

? Are you ready to proceed? (Y/n) y
```
次にfirebaseのどの機能を選ぶか選択するよう言われるので、**Firestore**,**Functions**,**Hosting**,**Storage**を選択してEnter。
(十字キーで移動、spaceで選択できます。)  
```
? Are you ready to proceed? Yes
? Which Firebase features do you want to set up for this directory? Press Space to selec
t features, then Enter to confirm your choices. 
 (*) Storage: Configure a security rules file for Cloud Storage
 ( ) Emulators: Set up local emulators for Firebase products
 ( ) Remote Config: Configure a template file for Remote Config
>( ) Realtime Database: Configure a security rules file for Realtime Database and (optio
nally) provision default instance
 (*) Firestore: Configure security rules and indexes files for Firestore
 (*) Functions: Configure a Cloud Functions directory and its files
 (*) Hosting: Configure files for Firebase Hosting and (optionally) set up GitHub Action
 deploys
 ( ) Hosting: Set up GitHub Action deploys
(Move up and down to reveal more choices)
```
次にプロジェクトセットアップに移る。すでにfirebase側でプロジェクトを作成してあるので、`Use an existing project`を選択する。
```
=== Project Setup
First, let's associate this project directory with a Firebase project.
You can create multiple project aliases by running firebase use --add, 
but for now we'll just set up a default project.

? Please select an option: (Use arrow keys)
> Use an existing project 
  Create a new project 
  Add Firebase to an existing Google Cloud Platform project 
  Don't set up a default project 
```
選択すると既存のプロジェクトを選ぶことができるので、適宜プロジェクトを選択。(ここではdemoプロジェクトを選んだ)
```
? Select a default Firebase project for this directory: (Use arrow keys)
> fir-9461b (demo) 
  sake-visualizer (SAKE Visualizer) 
```

次にfirestore setupに移る。特に記載せずenterでok
```
=== Firestore Setup

Firestore Security Rules allow you to define how and when to allow
requests. You can keep these rules in your project directory
and publish them with firebase deploy.

? What file should be used for Firestore Rules? (firestore.rules) 
? What file should be used for Firestore indexes? (firestore.indexes.json)
```
Functions Setupでは、Cloud Functionsで用いる言語を選ぶ。今回はTypeScript。
```
=== Functions Setup

A functions directory will be created in your project with sample code
pre-configured. Functions can be deployed with firebase deploy.

? What language would you like to use to write Cloud Functions? 
  JavaScript 
> TypeScript 
```
ESLintはyes。[ESLintって？](https://qiita.com/mzmz__02/items/63f2624e00c02be2f942)
```
? Do you want to use ESLint to catch probable bugs and enforce style? (Y/n) y
+  Wrote functions/package.json
+  Wrote functions/.eslintrc.js
+  Wrote functions/tsconfig.json
+  Wrote functions/tsconfig.dev.json
+  Wrote functions/src/index.ts
+  Wrote functions/.gitignore
? Do you want to install dependencies with npm now? (Y/n) y
```
Hosting Setupは以下のように設定。
```
=== Hosting Setup

Your public directory is the folder (relative to your project directory) that
will contain Hosting assets to be uploaded with firebase deploy. If you
have a build process for your assets, use your build's output directory.

? What do you want to use as your public directory? (public) <- enter
? Configure as a single-page app (rewrite all urls to /index.html)? (y/N) <- y
? Set up automatic builds and deploys with GitHub? (y/N) <- N
? File public/index.html already exists. Overwrite? (y/N) <- N
```
Storage Setupも基本Enterでok
```
=== Storage Setup

Firebase Storage Security Rules allow you to define how and when to allow
uploads and downloads. You can keep these rules in your project directory
and publish them with firebase deploy.

? What file should be used for Storage Rules? (storage.rules) <- enter
```

以下のようにDoneが出れば作成完了。
```

i  Writing configuration info to firebase.json...
i  Writing project information to .firebaserc...

+  Firebase initialization complete!
Done in 629.13s.
PS C:\Users\Yuki\Desktop\demo> 
```

#### 6.4. Reactのプロジェクトとfirebaseのプロジェクトを紐づける
ここではfirestoreからデータを読み取る部分、hostingを用いてスマホからアプリを見れるように設定していく。

##### 6.4.1. Firestore
最終目標として、firestoreにあるデータを取得して表示できるまで行う。  
- 必要なパッケージ(firebase)のインストール  
```
PS C:\Users\Yuki\Desktop\demo> yarn add firebase
```
プロジェクト内でfirestoreを作成していない場合は以下「データベースの作成」ボタンより作成
![firestore-home](/uploads/f80b43f4ca445c747a5f571426e4551a/firestore-home.png)  
ボタンを押すと本番環境モードで開始するかテストモードで開始するか選べるので、テストモードを選ぶ。Cloud Firestoreのロケーションはデフォルトのままでok。  

プロジェクトの概要の隣にある歯車ボタンを押し、「プロジェクトの設定」を開く。  
![firebase-key-setting-button](/uploads/b77c5a70ed109027a07a05f1fdea6200/firebase-key-setting-button.png)  

最初は「プロジェクトにはアプリがありません」と表示されるので、Webアプリの作成のため[</>]アイコンボタンを押す。  
![firebase-myapp-make](/uploads/062e9b4e89188ee8090053c4c4cf0b28/firebase-myapp-make.png)  

アプリの登録に進むので、アプリのニックネームを任意に付け、Firebase Hostingのチェックボックスもクリックして「アプリを登録」ボタンを押す。以降の2,3,4番は特に操作はなく進んでok。  
![firebase-myapp-register](/uploads/2b7f522c67bcb67b06b7be04ddc1ef04/firebase-myapp-register.png)  

完了すると、「プロジェクトの設定」の中に以下のような記載が追加される。  
このコードを用いることでfirestore等との連携を行うことができる。
![firebase-key-setting-key](/uploads/3f8fd956e2ae71f3b41207e464fa7973/firebase-key-setting-key.png)  

最後に、ダミーデータとしてfirestoreに適当なデータを入れてみる。左の構築から「firestore database」をクリックすると、以下のようなページが表示される。ここがNoSQLのデータベースになる。(firestoreの仕組みについては時間があればイシューにまとめます。[公式ドキュメント](https://firebase.google.com/docs/firestore?hl=ja))  
![firestore-data](/uploads/bf6ae9beaaf6e633301f85f0b2881dd9/firestore-data.png)  

コレクションを開始ボタンを押す。コレクションIDはPCでいうフォルダ名みたいなもの。 ここでは日本酒としてみる。
![firestore-collection](/uploads/41453a376cf1eebdc82c9ded8de15147/firestore-collection.png)  

次にドキュメントの登録になる。ドキュメントはPCでいうフォルダ内のファイルみたいなもの。ドキュメントIDは自動IDでも任意に付けても問題ない。今回は適当にお酒の名前と産地を格納してみる。  
![firestore-document](/uploads/a4742d32b9b203d83b0b5be069b64e62/firestore-document.png)  

以下のようにデータベースが作成されていればok。firebaseコンソール側での操作は以上になり、次にこのデータをReact側から読み込んでみる。
![firestore-result](/uploads/4939dd9ee43dbddaa638ba17a23dc804/firestore-result.png)  

- Reactからfirestoreのデータを読み取る  
まずプロジェクトの中にある`src`フォルダ内に`firebase.ts`というtsファイルを作成する。作成した`firebase.ts`内に下のコードをCopy&Pasteする。firebaseConfig部分は自身のfirebaseプロジェクトの「プロジェクトの設定」で表示されている文字列を記載する。ここではfirebaseConfigをもとにプロジェクト内のfirebaseなどの情報のやり取りをできるように設定している。最後の文章に`export { app, db }`とあるが、この記載をすることで他ファイルからdb(firestore内のデータベース)を読みこむことができるようになっている。
```
// firebase.ts内
// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getFirestore } from "firebase/firestore";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "",
  authDomain: "",
  projectId: "",
  storageBucket: "",
  messagingSenderId: "",
  appId: "",
  measurementId: ""
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getFirestore(app);

export { app, db }
```

次に実際にfirestoreのデータを読み込んで表示させてみる。  
今回は表示させることが目的のため、`App.tsx`に直接記載していく。  
```
// 変更前
import React from 'react';
import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.tsx</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;
```

まず前で作成した`firebase.ts`からdbをimportする。reactでは`import {変数名等} from {ファイル名}`の形で記載することでファイル名でexport指定してある変数等を呼び出すことができる。今回は以下のコードを`App.tsx` 上部に記載
```
import { db } from './firebase';
```
さらに、データベースからドキュメントを取得するため、追加で以下をimportする。
```
import { collection, query, getDocs } from "firebase/firestore";
import { useState } from 'react';
```

次に、実際にfirestoreのデータベースからデータを読み込んでみる。  
`App.tsx`内の`function App(){`の上に以下のようにデータを取り出すプログラムを記載  
(プログラムの中身は公式ドキュメントそのままですが最初はわからないと思うので、これどんな意味？っていうのあればイシューのほうに適当に書いてもらえればと思います！)
```
const getData:any = async (setData:any) => {
  const q = query(collection(db, "日本酒"));
  const querySnapshot = await getDocs(q);
  querySnapshot.forEach((doc) => {
    console.log(doc.id, " => ", doc.data()["名前"]);
    setData(doc.data()["名前"])
  });
}
```

function App()内は以下
```
function App() {
  const [data, setData] = useState()
  getData(setData)
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.tsx</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
        {data}
      </header>
    </div>
  );
}
```

このように書き換えてターミナルで`yarn start`する。
```
PS C:\Users\Yuki\Desktop\demo> yarn start
```
以下のようにfirestoreに格納した情報が出力されていればok。  
![react-demo-page](/uploads/5e6b3a6f8064a379dec3be0a42996831/react-demo-page.png)  

```
//変更後コード
import React from 'react';
import logo from './logo.svg';
import './App.css';
import { db } from './firebase';
import { collection, query, getDocs } from "firebase/firestore";
import { useState } from 'react';

const getData:any = async (setData:any) => {
  const q = query(collection(db, "日本酒"));
  const querySnapshot = await getDocs(q);
  querySnapshot.forEach((doc) => {
    console.log(doc.id, " => ", doc.data()["名前"]);
    setData(doc.data()["名前"])
  });
}

function App() {
  const [data, setData] = useState()
  getData(setData)
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.tsx</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
        {data}
      </header>
    </div>
  );
}

export default App;

```

##### 6.4.2. Hosting 
最終目標として、`https://{プロジェクトID}.web.app`といったurlにアクセスすることで、スマホ経由で開発したWebアプリを見れるようにする。  

- firebase.jsonの"hosting"内"public"を以下のように変更
``` 
// 変更前
  "hosting": {
    "public": "public",
    "ignore": [
      "firebase.json",
      "**/.*",
      "**/node_modules/**"
    ],
```

```
//変更後
  "hosting": {
    "public": "build",
    "ignore": [
      "firebase.json",
      "**/.*",
      "**/node_modules/**"
    ],
```
- ターミナルで以下のようにbuildとdeploy
```
PS C:\Users\Yuki\Desktop\demo> npm run build
PS C:\Users\Yuki\Desktop\demo> yarn firebase deploy
yarn run v1.22.17
$ C:\Users\Yuki\Desktop\demo\node_modules\.bin\firebase deploy

=== Deploying to 'fir-9461b'...

i  deploying storage, firestore, functions, hosting
Running command: npm --prefix "$RESOURCE_DIR" run lint

> lint
> eslint --ext .js,.ts .
i  hosting[fir-9461b]: releasing new version...
+  hosting[fir-9461b]: release complete

+  Deploy complete!

Project Console: https://console.firebase.google.com/project/fir-9461b/overview
Hosting URL: https://fir-9461b.web.app
Done in 154.01s.
```
このようにデプロイができれば、Hosting URLをスマホで記載することでアクセスができる。  
デプロイするにあたり、functionsでエラーが出る場合があるが、functions含めてデプロイする場合はプランがSparkプランではなくBlazeプランである必要があるため。アカウントのプランをBlazeプランにすることでデプロイ可能。

- Hosting停止
Hostingは以下のコマンドで停止できる。使わないときは基本停止しておき、開発中はhostingするのではなくローカルでみる。 
```
PS C:\Users\Yuki\Desktop\demo> yarn firebase hosting:disable
yarn run v1.22.17
$ C:\Users\Yuki\Desktop\demo\node_modules\.bin\firebase hosting:disable
? Are you sure you want to disable Firebase Hosting for the site fir-9461b
This will immediately make your site inaccessible! Yes
+  Hosting has been disabled for fir-9461b. Deploy a new version to re-enable.
Done in 9.10s.
```




### 7. その他技術的課題やエラーについて
GitLab内にあるイシューにまとめていければなと思ってます。適宜調べた内容をイシューに載せていく感じなら共有しやすそうだなと思うので。

