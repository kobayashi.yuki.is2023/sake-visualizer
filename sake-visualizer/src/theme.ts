import { extendTheme } from '@chakra-ui/react'

export const primaryTextColor = 'blue.400'
export const primaryColor = 'blue.300'

export const theme = extendTheme({
    styles: {
        global: {
            body: {
                bg: 'gray.100',
                color: 'gray.900',
                minHeight: '100vh',
            },
        },
        colors: {
            primaryText: '#319865',
            primary: '#319865',
        },
    },
})
