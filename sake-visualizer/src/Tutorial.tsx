import React, { useState, useMemo } from "react";
// import { Button } from "@mui/material";
import Modal from "react-modal";
//import { Container } from "@chakra-ui/react";
import { VscChromeClose } from 'react-icons/vsc';

import 'bootstrap/dist/css/bootstrap.min.css';
import Button from 'react-bootstrap/Button';

import { Center, Heading, VStack, Text, Image } from "@chakra-ui/react";

import SwipeableViews from "react-swipeable-views";
import { makeStyles } from "@material-ui/core/styles";

import homeImg from "./img/home.png";
import takePhotoImg from "./img/takePhoto.png";
import clickTextImg from "./img/ClickText.png";
import sakeInfoImg from "./img/sakeInfo.png";

const useStyles = makeStyles(() => {
    const baseStyle = {
        height: "100%",
        padding: 15,
        color: "black"
    };

    return {
        slide0: {
            ...baseStyle,
            backgroundColor: "silver"
        },
        slide1: {
            ...baseStyle,
            backgroundColor: "silver"
        },
        slide2: {
            ...baseStyle,
            backgroundColor: "silver"
        }
    };
});

interface Props {
    isClickButton: any;
    setIsClickButton: any;
}


export const Tutorial: React.VFC<Props> = (
    { isClickButton, setIsClickButton }
) => {
    const [swipeableActions, setSwipeableActions] = React.useState();
    const classes = useStyles();

    const slideContainer = {
        padding: '0 1px'
    };
    const rootStyle = {
        height: "85%",
        padding: '0 2px'
    };

    return (
        <>
            {/**ボタンをクリックしたときに表示されるWindow部分 */}
            <Modal
                isOpen={isClickButton}
                style={{
                    overlay: {
                        zIndex: 100
                    }
                }}
                onRequestClose={() => setIsClickButton(false)}
                ariaHideApp={false}
            >
                <Center>
                    <Heading>使い方</Heading>
                </Center>
                <SwipeableViews
                    enableMouseEvents
                    resistance
                    animateHeight
                    style={rootStyle}
                    slideStyle={slideContainer}
                    containerStyle={{ height: '100%', padding: '0 2px', }}
                >
                    <div className={classes[`slide0`]}>
                        <Center>
                            <VStack py={2}>
                                <Text fontSize='5xl'>1. 写真を撮るボタンをポチ！</Text>
                                <Image src={homeImg} boxSize={'90%'}></Image>
                            </VStack>
                        </Center>
                    </div>
                    <div className={classes[`slide1`]}>
                        <Center>
                            <VStack py={2}>
                                <Text fontSize='5xl'>2. メニューを撮影！</Text>
                                <Image src={takePhotoImg} boxSize={'90%'}></Image>
                            </VStack>
                        </Center>
                        <Text>※iPhoneの場合はURLに隠れて撮影ボタンが見えない場合があります。下に少しスクロールすると出てきます。</Text>
                    </div>
                    <div className={classes[`slide2`]}>
                        <Center>
                            <VStack py={2}>
                                <Text fontSize='5xl'>3. AIが認識したお酒をタップ！</Text>
                                <Image src={clickTextImg} boxSize={'90%'}></Image>
                            </VStack>
                        </Center>
                    </div>
                    <div className={classes[`slide2`]}>
                        <Center>
                            <VStack py={2}>
                                <Text fontSize='5xl'>4. お酒の情報が見れる！</Text>
                                <Image src={sakeInfoImg} boxSize={'90%'}></Image>
                            </VStack>
                        </Center>
                    </div>
                    <div className={classes[`slide2`]}>
                        <Center>
                            <VStack py={2}>
                                <Text fontSize='5xl'>使い方はこれだけ！</Text>
                                <Text fontSize='5xl'>最後に、</Text>
                                <Text fontSize='5xl'>まだ登録されていないお酒は情報が出てきません泣</Text>
                                <Text fontSize='5xl'>順次追加予定です！</Text>
                                <br />
                                <Text fontSize='5xl'>AIの精度が悪くうまく表示されない場合があります。</Text>
                                <Text fontSize='5xl'>その際は再度取り直し、できるだけ大きく文字を映してみてください！</Text>
                                <br />
                                <Text fontSize='5xl'>下のボタンから始めましょう！</Text>
                                <Button style={{ backgroundColor: "#319865", borderColor: "#319865" }} onClick={() => setIsClickButton(false)}> 始める！</Button>
                            </VStack>
                        </Center>
                    </div>
                </SwipeableViews>
            </Modal>
        </>
    )
}