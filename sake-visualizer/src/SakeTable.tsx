import React from 'react';

import sake_img from './img/vodka.png';
import ListGroup from 'react-bootstrap/ListGroup';
import Card from 'react-bootstrap/Card';
import { Center } from "@chakra-ui/react";
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Table from 'react-bootstrap/Table';
import { Heading, VStack, Text, Image } from "@chakra-ui/react";



export const SakeTable: React.VFC<any> = ({ data }: any) => {
    const zairyouData = data["材料"]
    const zairyou = []
    if (zairyouData !== undefined) {
        for (var i = 1; i < zairyouData.length; i = i + 2) {
            zairyou.push(
                <tr>
                    <td>{zairyouData[i - 1]}</td>
                    <td>{zairyouData[i]}</td>
                </tr>
            )
        }
    }


    return (
        <>
            <Container fluid>
                {
                    data["img"] == null ?
                        null
                        :
                        <Image width={"100%"} src={data["img"]} />
                }
                <Row>
                    <Container fluid>
                        <Card border="primary">
                            <Card.Body>
                                <Center>
                                    {data["名前"] !== false ?
                                        <Card.Title><h1>{data["名前"]}</h1></Card.Title>
                                        : null}
                                </Center>
                                <Card.Text>
                                    {data["説明"]}
                                </Card.Text>
                            </Card.Body>
                            <ListGroup variant="flush">
                                <ListGroup.Item>度数：{data["度数"]}</ListGroup.Item>
                                <ListGroup.Item>味：{data["味"]}</ListGroup.Item>
                                {data["おすすめ"] ?
                                    <ListGroup.Item>こんな人におすすめ！：{data["おすすめ"]}</ListGroup.Item>
                                    : null}
                            </ListGroup>
                        </Card>
                    </Container>
                </Row>
                <Row>
                    <Col>
                        {data["種類"] == "カクテル" ?
                            //カクテル専用
                            <Table striped bordered hover>
                                <thead>
                                    <tr>
                                        <th>材料</th>
                                        <th>分量</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {zairyou}
                                </tbody>
                            </Table>
                            :
                            //それ以外
                            <Table striped bordered hover>
                                <tbody>
                                    {zairyou}
                                </tbody>
                            </Table>
                        }
                    </Col>
                </Row>
            </Container>

        </>
    )
}