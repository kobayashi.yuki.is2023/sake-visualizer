import React, { useState, useMemo } from "react";
// import { Button } from "@mui/material";
import Modal from "react-modal";
//import { Container } from "@chakra-ui/react";
import { VscChromeClose } from 'react-icons/vsc';

import 'bootstrap/dist/css/bootstrap.min.css';
import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';

import { db } from './firebase';

import { collection, query, where, getDocs } from 'firebase/firestore';

import { Center, Heading, VStack, Text } from "@chakra-ui/react";

import { SakeTable } from './SakeTable';
import { UserInfo } from './UserInfo';


interface Props {
    isClickButton: any;
    setIsClickButton: any;
}

//受け取るデータの型
interface dataProps {
    名前: string;
    味: string;
    度数: string;
    説明: string;
    img: string;
    材料?: any;
    おすすめ?: string;
}


export const SakeInfoWindow: React.VFC<Props> = (
    { isClickButton, setIsClickButton }
) => {

    const [data, setData] = useState<dataProps>();
    useMemo(() => {
        const getData: any = async (setData: any) => {
            console.log(isClickButton)
            const q = query(collection(db, "sake"), where("名前", "==", isClickButton));
            const querySnapshot = await getDocs(q);
            querySnapshot.forEach((doc) => {
                // doc.data() is never undefined for query doc snapshots
                console.log(doc.id, " => ", doc.data());
                setData(doc.data())
            });
        }
        getData(setData);
    }, [isClickButton]);


    return (
        <>
            {/**ボタンをクリックしたときに表示されるWindow部分 */}
            <Modal
                isOpen={isClickButton != "false"}
                style={{
                    overlay: {
                        zIndex: 100
                    }
                }}
                onRequestClose={() => setIsClickButton("false")}
                ariaHideApp={false}
            >

                {/**Close Button */}
                <Button style={{ backgroundColor: "#319865", borderColor: "#319865" }} onClick={() => setIsClickButton("false")}>
                    <VscChromeClose />
                </Button>

                {/**Window内のテキスト部分*/}
                {data !== undefined ?
                    data["名前"] !== "false" ?
                        data["名前"] == "小林優輝" || data["名前"] == "城谷知葵" || data["名前"] == "観音寺碧斗" ?
                            <Center>
                                <UserInfo data={data} />
                            </Center>
                            :
                            <Center>
                                <SakeTable data={data} />
                            </Center>
                        :
                        <Center>
                            <VStack py={2}>
                                <Heading>申し訳ありません！</Heading>
                                <Heading>「{isClickButton}」は登録されておりません</Heading>
                            </VStack>
                        </Center>
                    : null}
            </Modal>
        </>
    )
}