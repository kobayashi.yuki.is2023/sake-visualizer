import React, { useState } from 'react';
import Camera, { FACING_MODES } from 'react-html5-camera-photo';
import 'react-html5-camera-photo/build/css/index.css';
import { Center, Heading, VStack, Text } from "@chakra-ui/react";
import sake_img from './img/sake.jpg';
import useImage from 'use-image';
import Button from 'react-bootstrap/Button';

import { Layer, Rect, Stage, Image, Line } from "react-konva";

import { SakeInfoWindow } from './SakeInfoWindow';

interface ocrInfo {
    text: string
    position: any
}


export const TakePhoto = (props: any) => {
    //撮影した写真の情報 base64でエンコードされている。
    const [dataURI, setDataURI] = useState<any>("")
    //Cloud Visionから返ってきたレスポンスを格納 認識結果の文字と位置が格納されている
    const [ocrText, setocrText] = useState<any>("")

    const [isTakePhoto, setIsTakePhoto] = useState(false)

    const handleTakePhoto = async (dataUri: any) => {
        setDataURI(dataUri)
        sendCloudVision(dataUri.slice(22))
        setIsTakePhoto(true)
    }

    const [imgHeight, setImgHeight] = useState<number>(0)
    const [imgWidth, setImgWidth] = useState<number>(0)

    const [isClickText, setIsClickText] = useState("false")

    const sendCloudVision = async (image: string) => {
        const body = JSON.stringify({
            requests: [
                {
                    features: [{ type: "TEXT_DETECTION", maxResults: 1 }],
                    image: {
                        content: image
                    }
                }
            ]
        });

        const response = await fetch(
            "https://vision.googleapis.com/v1/images:annotate?key=" + process.env.REACT_APP_GOOGLE_CLOUD_VISION_API_KEY,
            {
                headers: {
                    Accept: "application/json",
                    "Content-Type": "application/json"
                },
                method: "POST",
                body: body
            }
        );
        const ocr = await response.json()
        //認識したテキスト情報
        const text = ocr["responses"][0].fullTextAnnotation.text.slice(0, -3).split(/\n/);
        //認識したテキストの位置情報
        const textPosition = ocr["responses"][0].fullTextAnnotation.pages[0].blocks.map(
            (data: any) => {
                return data.boundingBox.vertices
            })

        //写真の高さと幅の取得
        setImgHeight(ocr["responses"][0].fullTextAnnotation.pages[0].height)
        setImgWidth(ocr["responses"][0].fullTextAnnotation.pages[0].width)

        const ocrText = []
        for (var i = 0; i < text.length; i++) {
            const data: ocrInfo = { text: text[i], position: textPosition[i] }
            ocrText.push(data)
        }

        console.log("position -> ")
        console.log(textPosition)
        console.log("text -> ")
        console.log(text)
        console.log("ocrText -> ")
        console.log(ocrText)
        setocrText(ocrText)
    }

    const [image] = useImage(dataURI);
    const lineList = []

    for (var i = 0; i < ocrText.length; i++) {
        const text = ocrText[i].text
        if (ocrText[i].position !== undefined) {
            lineList.push(
                <>
                    <Line
                        x={ocrText[i].position[0].x}
                        y={ocrText[i].position[0].y}
                        points={[
                            0,
                            0,
                            ocrText[i].position[1].x - ocrText[i].position[0].x,
                            ocrText[i].position[1].y - ocrText[i].position[0].y,
                            ocrText[i].position[2].x - ocrText[i].position[0].x,
                            ocrText[i].position[2].y - ocrText[i].position[0].y,
                            ocrText[i].position[3].x - ocrText[i].position[0].x,
                            ocrText[i].position[3].y - ocrText[i].position[0].y,
                        ]}
                        closed
                        stroke="yellow"
                        strokeWidth={5}
                        fillLinearGradientStartPoint={{ x: -50, y: -50 }}
                        fillLinearGradientEndPoint={{ x: 50, y: 50 }}
                        onClick={() => setIsClickText(text)}
                        onTap={() => setIsClickText(text)}
                    />
                </>
            )
        }
    }

    return (
        <>
            {isTakePhoto === false ?
                <>
                    <Center scale={"0.8"}>
                        <Camera
                            sizeFactor={0.9}
                            onTakePhoto={(dataUri) => { handleTakePhoto(dataUri); }}
                            idealFacingMode={FACING_MODES.ENVIRONMENT}
                            isMaxResolution={true}
                            isFullscreen={true}
                            //PCカメラを使う場合はisImageMirrorをfalseに
                            isImageMirror={false}
                        />
                    </Center>
                </>
                : ocrText !== "" ?
                    <>
                        <Stage width={imgWidth} height={imgHeight} draggable={true}>
                            <Layer>
                                <Image image={image} width={imgWidth} height={imgHeight} />
                                {lineList}
                            </Layer>
                        </Stage>
                        <SakeInfoWindow isClickButton={isClickText} setIsClickButton={setIsClickText} />

                        <Button style={{ backgroundColor: "#319865", borderColor: "#319865" }} onClick={() => {
                            setocrText("")
                            setIsClickText("false")
                            setIsTakePhoto(false)
                        }}>
                            取り直す
                        </Button>
                    </>
                    : <Heading> 認識中... </Heading>
            }
        </>
    )
}

