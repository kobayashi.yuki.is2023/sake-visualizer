import React, { useState } from 'react';
import { Center, Heading, VStack, Text } from "@chakra-ui/react";
import 'bootstrap/dist/css/bootstrap.min.css';
import Button from 'react-bootstrap/Button';
import Image from 'react-bootstrap/Image';
import { AiOutlineCamera } from 'react-icons/ai';

import { Nav, NavItem, Dropdown, NavLink } from 'react-bootstrap';



//お酒の名前をクリックしたときに出てくるウィンドウ部分のコンポーネント
//テスト段階ではホームに設置されている仮のボタンもここで実装
import { SakeInfoWindow } from './SakeInfoWindow';
import { Tutorial } from './Tutorial';


export const HomeWindow: React.VFC<any> = ({ setIsClickTakePhotoButton }: any) => {
    const [isClickButton, setIsClickButton] = useState<string>("false")

    const [isClickTutorialButton, setIsClickTutorialButton] = useState(false)
    const width = window.innerWidth / 3
    return (
        <>
            <Heading style={{ background: "#31CA7D" }}><h1><em><strong><div className="p-3 mb-2">SAKE Visualizer</div></strong></em></h1></Heading>

            <Text><h4><em>メニューの写真を撮るだけで簡単にお酒の情報をあなたに提供します</em></h4></Text>
            <Image src="https://thumb.photo-ac.com/27/277817fd57b71bbc3a00aeca7af8380f_t.jpeg" fluid />

            <Button
                style={{
                    width: width, height: width, color: "white", outline: "none", backgroundColor: "#319865", borderRadius: "50%", boxShadow: "4px 4px", borderColor: "#319865"
                }}
                onClick={() => setIsClickTakePhotoButton(true)}
            >
                <AiOutlineCamera size={"70%"} />
            </Button>

            <Button
                style={{ width: "90%", backgroundColor: "#319865", outline: "none", boxShadow: 'none', borderColor: "#319865" }}
                onClick={() => setIsClickTutorialButton(true)} >
                <h4>
                    使い方
                </h4>
            </Button >
            <Tutorial isClickButton={isClickTutorialButton} setIsClickButton={setIsClickTutorialButton} />


            {/**以下Test用　本番は削除 
            < Button onClick={() => setIsClickButton("真澄")} >
                クリックするとお酒表示ウィンドウでます
            </Button >
            <SakeInfoWindow isClickButton={isClickButton} setIsClickButton={setIsClickButton} />
            */}
        </>
    )
}