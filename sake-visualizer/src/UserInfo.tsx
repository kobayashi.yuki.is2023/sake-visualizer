import React from 'react';

import sake_img from './img/vodka.png';
import ListGroup from 'react-bootstrap/ListGroup';
import Card from 'react-bootstrap/Card';
import { Center } from "@chakra-ui/react";
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Table from 'react-bootstrap/Table';
import { Heading, VStack, Text, Image } from "@chakra-ui/react";



export const UserInfo: React.VFC<any> = ({ data }: any) => {
    const zairyouData = data["材料"]
    const zairyou = []
    if (zairyouData !== undefined) {
        for (var i = 1; i < zairyouData.length; i = i + 2) {
            zairyou.push(
                <ListGroup.Item>{zairyouData[i - 1]}：{zairyouData[i]}</ListGroup.Item>
            )
        }
    }


    return (
        <>
            <Container fluid>
                {
                    data["img"] == null ?
                        null
                        :
                        <Image width={"100%"} src={data["img"]} />
                }
                <Row>
                    <Container fluid>
                        <Card border="primary">
                            <Card.Body>
                                <Center>
                                    {data["名前"] !== false ?
                                        <Card.Title><h1>{data["名前"]}</h1></Card.Title>
                                        : null}
                                </Center>
                                <Card.Text>
                                    {data["説明"]}
                                </Card.Text>
                            </Card.Body>
                            <ListGroup variant="flush">
                                <ListGroup.Item>出身：{data["度数"]}</ListGroup.Item>
                                <ListGroup.Item>趣味：{data["味"]}</ListGroup.Item>
                                {data["おすすめ"] ?
                                    <ListGroup.Item>こんな人におすすめ！：{data["おすすめ"]}</ListGroup.Item>
                                    : null}
                                {zairyou}
                            </ListGroup>
                        </Card>
                    </Container>
                </Row>
            </Container>

        </>
    )
}