import React, { useState } from 'react';
import './App.css';
import { Center, VStack } from "@chakra-ui/react";

//写真撮影画面のコンポーネント
import { TakePhoto } from './TakePhoto';

import { HomeWindow } from './HomeWindow';

function App() {
  //写真を撮るボタンがクリックされたかどうか  
  const [isClickTakePhotoButton, setIsClickTakePhotoButton] = useState(false)


  const Home = () => {
    //写真撮影画面に移るかどうかの選択
    //[写真を撮る］ボタンがクリックされたらHomeから写真撮影画面に切り替える
    if (isClickTakePhotoButton === false) {
      return (
        <HomeWindow
          setIsClickTakePhotoButton={setIsClickTakePhotoButton} />
      )
    } else {
      //写真撮影ボタンを押した際カメラ起動
      return (isClickTakePhotoButton === true ? <TakePhoto /> : null
      )
    }
  }

  return (
    <div className="App" >
      <Center style={{ background: "white", height: window.innerHeight }}>
        <VStack py={2}>
          <Home />
        </VStack>
      </Center>
    </div>
  );
}

export default App;
